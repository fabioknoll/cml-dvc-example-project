#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import tensorflow as tf
import seaborn as sns
import json
from sklearn.metrics import confusion_matrix, precision_score, recall_score, accuracy_score

import pathlib

tf.__version__


# ## Image Count

# In[2]:


data_dir = pathlib.Path("./data")
image_count = len(list(data_dir.glob('*/*.jpg')))
print(image_count)


# ## Dataset

# In[3]:


BATCH_SIZE = 32
IMG_SIZE = 180


# In[4]:


train_ds = tf.keras.utils.image_dataset_from_directory(
  data_dir,
  validation_split=0.3,
  subset="training",
  seed=123,
  image_size=(IMG_SIZE, IMG_SIZE),
  batch_size=BATCH_SIZE)

val_ds = tf.keras.utils.image_dataset_from_directory(
  data_dir,
  validation_split=0.3,
  subset="validation",
  seed=123,
  image_size=(IMG_SIZE, IMG_SIZE),
  batch_size=BATCH_SIZE)


# In[5]:


val_batches = tf.data.experimental.cardinality(val_ds)
test_ds = val_ds.take(val_batches // 2)
val_ds = val_ds.skip(val_batches // 2)


# In[6]:


class_names = train_ds.class_names
print(class_names)


# ### Input Shapes

# In[7]:


for image_batch, labels_batch in train_ds:
  print(image_batch.shape)
  print(labels_batch.shape)
  break


# ### Enable Caching and Prefetching

# In[8]:


AUTOTUNE = tf.data.AUTOTUNE

train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)


# ## Model

# In[9]:


num_classes = 2

model = tf.keras.Sequential([
  tf.keras.layers.Rescaling(1./255, input_shape=(IMG_SIZE, IMG_SIZE, 3)),
  tf.keras.layers.Conv2D(16, 3, padding='same', activation='relu'),
  tf.keras.layers.MaxPooling2D(),
  tf.keras.layers.Conv2D(32, 3, padding='same', activation='relu'),
  tf.keras.layers.MaxPooling2D(),
  tf.keras.layers.Conv2D(64, 3, padding='same', activation='relu'),
  tf.keras.layers.MaxPooling2D(),
  tf.keras.layers.Flatten(),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dense(num_classes)
])


# In[10]:


model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])


# In[11]:


model.summary()


# ## Train

# In[12]:


epochs=15
history = model.fit(
  train_ds,
  validation_data=val_ds,
  epochs=epochs
)


# ## Results

# In[13]:


predictions = np.array([])
labels =  np.array([])
for x,y in test_ds:
    y_pred = np.concatenate([predictions, np.argmax(model.predict(x), axis = -1)])
    y_true = np.concatenate([labels, y.numpy()])


# In[17]:


acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(12, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.savefig("accuracy_loss_curve.png")


# ## Confusion Matrix

# In[18]:


cm = confusion_matrix(y_true, y_pred, normalize = 'true')
fig, ax = plt.subplots(figsize=(8,7))
sns.heatmap(cm, annot=True, xticklabels=class_names, yticklabels=class_names)
plt.ylabel('Actual')
plt.xlabel('Predicted')
ax.set_ylim([0,2])
ax.invert_yaxis()
plt.savefig("./conf_mat.png")


# ## Metrics

# In[ ]:

accuracy = accuracy_score(y_true, y_pred)
precision = precision_score(y_true, y_pred)
recall = recall_score(y_true, y_pred)

with open("metrics.json","w") as file:
    json.dump({"accuracy": accuracy, "precision": precision, "recall": recall}, file)


# ## Export Model

# In[ ]:


model.save('./models/image_classification_model')

