# CI/CD ML Pipeline using DVC and CML

This is an example machine learning project using data version control (DVC) and continuous machine learning (CML) with a CI/CD Cloud Runner on Google Cloud Platform. The project was created in context of a blog post, written for the course "Big Data Processing" a part of the master program [Data Science & Intelligent Analytics](https://fh-kufstein.ac.at/Studieren/Master/Data-Science-Intelligent-Analytics-BB) at FH Kufstein Tirol.

You can check it out [here](http://big-data-processing.pages.web.fh-kufstein.ac.at/gitbook/fabio_knoll/ml_cicd_pipeline_cloud_knoll.html)!

